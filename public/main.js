function checkInp() {
    let inp = document.getElementById("quan").value;
    if (inp.match(/^[0-9]+$/) === null) {
        document.getElementById("price").innerHTML = "Неверные данные";
        return false;
    } else {
        return true;
    }
}

function getPrices() {
    return {
        Pizza: [250, 300, 350],
        saleOptions: {
            option1: 0,
            option2: 100,
            option3: 150,
        },
        saleProperties: {
            prop1: 100,
            prop2: 200,
        },
    };
}

function price() {
    let sel = document.getElementById("Pizza");
    let quan = document.getElementById("quan").value;
    let radioDiv = document.getElementById("radios");
    let boxes = document.getElementById("checkboxes");
    let price = document.getElementById("price");
    price.innerHTML = "0";
    switch (sel.value) {
        case "1":
            radioDiv.style.display = "none";
            boxes.style.display = "none";

            if (checkInp()) {
                let all = parseInt(quan) * getPrices().Pizza[0];
                price.innerHTML = all + " рублей";
            }
            break;
        case "2":
            radioDiv.style.display = "block";
            boxes.style.display = "none";
            let a;
            let rad = document.getElementsByName("saleOptions");
            for (var i = 0; i < rad.length; i++) {
                if (rad[i].checked) {
                    a = rad[i].value;
                }
            }
            let priOfOp = getPrices().saleOptions[a];
            if (checkInp()) {
                let all = parseInt(quan) * (getPrices().Pizza[1] + priOfOp);
                price.innerHTML = all + " рублей";
            }
            break;
        case "3":
            radioDiv.style.display = "none";
            boxes.style.display = "block";
            let sum = 0;
            let p = document.getElementsByName("prop");
            for (let i = 0; i < p.length; i++) {
                if (p[i].checked) {
                    sum += getPrices().saleProperties[p[i].value];
                }
            }

            if (checkInp()) {
                let all = parseInt(quan) * (getPrices().Pizza[2] + sum);
                price.innerHTML = all + " рублей";
            }
            break;
    }
}
window.addEventListener("DOMContentLoaded", function(event) {
    let radioDiv = document.getElementById("radios");
    radioDiv.style.display = "none";
    let boxes = document.getElementById("checkboxes");
    boxes.style.display = "none";

    let quan = document.getElementById("quan");
    quan.addEventListener("change", function(event) {
        console.log("quan was changed");
        price();
    });

    let select = document.getElementById("Pizza");
    select.addEventListener("change", function(event) {
        console.log("cfg was changed");
        price();
    });

    let radios = document.getElementsByName("saleOptions");
    radios.forEach(function(radio) {
        radio.addEventListener("change", function(event) {
            console.log("Size was changed");
            price();
        });
    });
    let checkboxes = document.querySelectorAll("#checkboxes input");
    checkboxes.forEach(function(checkbox) {
        checkbox.addEventListener("change", function(event) {
            console.log("Options was changed");
            price();
        });
    });
    price();
});